=begin
class Calculator
  def sum(number, other)
    number + other
  end
end
 calculator = Calculator.new
 puts calculator.sum(2,3)

class UpdatedCalculator
  def plus(number, other)
    number + other
  end

  def minus(number, other)
    number - other
  end

  def multiply(number, other)
    number * other
  end

  def divide(number, other)
    number.to_f / other
  end
end

calculator = UpdatedCalculator.new
puts calculator.plus(3,3)
puts calculator.minus(2, 3)
puts calculator.multiply(2, 3)
puts calculator.divide(2, 3)
=end 

=begin
class Person

  def initialize(name)
    @name = name
  end

  def name 
    @name
  end 

  def password=(password)
    @password = password 
  end

  def greet(other)
    puts "Hi " + other.name + "! " + "My name is " + name + "."
  end

end 
=end

=begin
person = Person.new("Anja")
#person.password= ("realize")
friend = Person.new("Carla")

person.greet(friend)
friend.greet(person)
=end


#Writing out classes, defining methods, defining instance methods, initializing objects, instance variables
# attribute writers, state and behavior ruby tutorial. 

#instantiating an object and let it interact with other objects

=begin 
class Person
  def initialize(name)
    @name = name
    p self
  end
end

person = Person.new("Anja")
p person
=end

=begin 
5.times do 
  puts "oh hi friend"
end 
=end 

#A piece of code that accepts arguments, and returns value. A block is always passed to a method call.

=begin
5.times do 
  puts "oh hello"
end 

5.times {puts "hello!"}
=end

=begin
[1, 2, 3, 4, 5].each { |number| puts "#{number} was passed to the block" }
=end 

=begin 
number = 5

if number.between?(1, 10)
  puts "The number is between 1 and 10"
elsif number.between?(11, 20)
  puts "The number is between 11 and 20"
else
  puts "The number is bigger than 20"
end
=end 

#Conditional statement above